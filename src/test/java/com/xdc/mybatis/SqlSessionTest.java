package com.xdc.mybatis;

import com.xdc.mapper.EmployeeMapper;
import com.xdc.model.Employee;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;


public class SqlSessionTest{
    private static final Logger logger = LoggerFactory.getLogger(SqlSessionTest.class);

    SqlSession sqlSession;

    @Before
    public void beforeClass() throws IOException {
        logger.info("beforeClass");
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        sqlSession = sessionFactory.openSession();
    }

    @After
    public void afterClass() {
        logger.info("afterClass");
        sqlSession.close();
    }

    @Test
    public void testSession01() throws Exception {
        EmployeeMapper employeeMapper = sqlSession.getMapper(EmployeeMapper.class);
        Employee e = employeeMapper.findByEmployeeNo(10002);
        logger.info("{}", e);
    }
}