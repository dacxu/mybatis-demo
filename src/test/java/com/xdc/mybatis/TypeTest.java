package com.xdc.mybatis;

import org.apache.ibatis.io.ResolverUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.Set;


class Person {
    protected static final Logger logger = LoggerFactory.getLogger(Person.class);

    public void say() {
        Type genericSuperclass = this.getClass().getGenericSuperclass();
        logger.info("{}: {}", this.getClass().getName(), genericSuperclass);

        if(genericSuperclass instanceof Class) {
            logger.info("class: {}", (Class)genericSuperclass);
        }
    }
}
class Man extends Person {
//    public void say() {
//        logger.info("Man: {}", this.getClass().getGenericSuperclass());
//    }
}

class Search<M, R> {
    private static final Logger logger = LoggerFactory.getLogger(Search.class);
    
    public void say() {
        Type genericSuperclass = this.getClass().getGenericSuperclass();
        logger.info("{}: {}", this.getClass().getName(), genericSuperclass);

        if(genericSuperclass instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) genericSuperclass;
            logger.info("ParameterizedType: {}", parameterizedType);

            Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
            for (Type type : actualTypeArguments) {
                logger.info("arguments: {}", type);
            }
        }
    }
}

class BioSearch extends Search<String, Map<String, Integer>> {

}

public class TypeTest {
    private static final Logger logger = LoggerFactory.getLogger(TypeTest.class);


    @Before
    public void beforeClass() throws IOException {
    }

    @After
    public void afterClass() {
    }

    @Test
    public void testSession01() throws Exception {
        new Man().say();
        new Person().say();

        new BioSearch().say();
    }

    @Test
    public void testSession02() {
        ResolverUtil<Class<?>> resolverUtil = new ResolverUtil<Class<?>>();
        resolverUtil.find(new ResolverUtil.IsA(Object.class), "com.xdc.model");
        Set<Class<? extends Class<?>>> handlerSet = resolverUtil.getClasses();
        for (Class c : handlerSet) {
            logger.info("{}", c);
        }
    }
}