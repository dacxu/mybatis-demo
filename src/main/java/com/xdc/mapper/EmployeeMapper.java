package com.xdc.mapper;

import com.xdc.model.Employee;

/**
 * Author: xudacheng@patsnap.com
 * Date:   8/22/18 11:39 AM
 */
public interface EmployeeMapper {
    Employee findByEmployeeNo(Integer empNo);
}
