package com.xdc;

import com.xdc.mapper.EmployeeMapper;
import com.xdc.model.Employee;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;

/**
 * Hello world!
 *
 */
public class App 
{
    private static final Logger logger = LoggerFactory.getLogger(App.class);
    
    public static void main( String[] args ) throws IOException {
        //org.apache.ibatis.logging.LogFactory.useSlf4jLogging();

        //mybatis的配置文件
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
//        byte buffer[]=new byte[100];
//        while(inputStream.read(buffer,0,100)!=-1){//-1表示读取结束
//            System.out.print(new String(buffer));
//        }

        SqlSessionFactory sessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession session = sessionFactory.openSession();

        EmployeeMapper employeeMapper = session.getMapper(EmployeeMapper.class);
        Employee e = employeeMapper.findByEmployeeNo(10002);
        logger.info("{}", e);
    }
}

