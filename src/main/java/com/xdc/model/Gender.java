package com.xdc.model;

/**
 * Author: xudacheng@patsnap.com
 * Date:   8/22/18 2:19 PM
 */
public enum Gender {
    M("male"),
    F("female");

    private String value;

    Gender(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}